# php-demo

## Setting up the project

```bash
docker run --name php --network php-nw -dp 8080:80 \         
-v ./:/var/www/html \
-e MYSQL_ROOT_PASSWORD=secret \
-e MYSQL_USERNAME=root \
-e MYSQL_PASSWORD=secret \
-e MYSQL_DATABASE=demos \
-e MYSQL_HOST=mysql \
php-demo:2.0
```

```bash
docker run --name phpmyadmin -d --network php-nw -e PMA_HOST=mysql -p 8081:80 phpmyadmin:latest
```

```bash
docker run --name mysql -d -p 3307:3306 \                                                      
--network php-nw --network-alias mysql \
-v php-sql:/var/lib/mysql \
  -e MYSQL_ROOT_PASSWORD=secret \
  -e MYSQL_DATABASE=demos \
mysql:8.0
```