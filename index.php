<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Movies List</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            padding: 20px;
        }

        .movie {
            border: 1px solid #ccc;
            border-radius: 5px;
            margin-bottom: 20px;
            padding: 10px;
            background-color: #fff;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
        }

        .movie strong {
            color: #333;
        }
    </style>
</head>
<body>

<?php
$servername = "mysql";
$username = "root";
$password = "secret";
// Create connection
$conn = mysqli_connect($servername, $username, $password);
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
$sql = 'SELECT title, genre, director, release_year FROM movies';
mysqli_select_db($conn, 'demos');
$retval = mysqli_query($conn, $sql);
if (!$retval) {
    die('Could not get data: ' . mysqli_error());
}

while ($row = mysqli_fetch_assoc($retval)) {
    echo "<div class='movie'>
            <strong>Title:</strong> {$row['title']} <br>
            <strong>Genre:</strong> {$row['genre']} <br>
            <strong>Director:</strong> {$row['director']} <br>
            <strong>Release Year:</strong> {$row['release_year']} <br>
          </div>";
}
mysqli_close($conn);
?>

</body>
</html>
